/*
 *  This file is part of pynetworkred.
 *
 *  Copyright (C) 2019, University of Copenhagen
 *  https://bitbucket.org/stefansommer/pynetworkred
 *  
 *  pynetworkred is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  pynetworkred is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with networkred.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>
// Thevenin functionality and macros
#include "thevenin_module.hpp"
#include "thevenin/thevenin_klu.hpp"
#include "thevenin/thevenin_klu_parkronred.hpp"
#include "thevenin/thevenin_reach.hpp"
#include "thevenin/thevenin_getz.hpp"
// Packages for assisting parsing and packaging PyObjects
#include "parse.hpp"
#include "package.hpp"

// Input args: n, M, L, U, Link, LinkT, D, Pone, Qone, reachL_double, reachU_double, diagOnly
static PyObject *thevenin_getz_wrapper(PyObject* self, PyObject* args) {
	// Placeholder for each item
	PyObject *pPy;
    int i = 0;
    // Allocation for input args
    int n, M;
    complex_sparse_matrix L, U, Link, LinkT, D;
    std::vector<int> Pone, Qone;
    bool diagOnly;
    // Allocation of temporary placeholder used in thevenin_getz call
    std::vector<std::complex<double>> v;
    // Loading in all the arguments
    PyObject *vfast;

	// Grab the first argument (Nnvc)
	pPy = PyTuple_GetItem(args,i++);
	// Parse (integer) number of iterations
	if (!parse_integer(pPy, &n) == 0) {
		return NULL;
	};

	// Grab the second argument (Nvc)
	pPy = PyTuple_GetItem(args,i++);
	// Parse (integer) number of iterations
	if (!parse_integer(pPy, &M) == 0) {
		return NULL;
	};

	// Grab the third argument (csr_matrix Link)
	pPy = PyTuple_GetItem(args,i++);

	// Parse (csr_matrix) L
	if (!parse_complex_sparse_matrix(pPy, L) == 0) {
		return NULL;
	};

	// Grab the fourth argument (csr_matrix U)
	pPy = PyTuple_GetItem(args,i++);

	// Parse (csr_matrix) U
	if (!parse_complex_sparse_matrix(pPy, U) == 0) {
		return NULL;
	};

    // Grab the fifth argument (csr_matrix Link)
	pPy = PyTuple_GetItem(args,i++);

	// Parse (csr_matrix) U
	if (!parse_complex_sparse_matrix(pPy, Link) == 0) {
		return NULL;
	};

    // Grab the sixth argument (csr_matrix LinkT)
	pPy = PyTuple_GetItem(args,i++);

	// Parse (csr_matrix) U
	if (!parse_complex_sparse_matrix(pPy, LinkT) == 0) {
		return NULL;
	};

    // Grab the seventh argument (csr_matrix D)
	pPy = PyTuple_GetItem(args,i++);

	// Parse (csr_matrix) U
	if (!parse_complex_sparse_matrix(pPy, D) == 0) {
		return NULL;
	};

    // Grab the eigth argument (Pone)
	pPy = PyTuple_GetItem(args,i++);

	// Parse ([integer]) Pone
	if (!parse_int_vector(pPy, Pone) == 0) {
		return NULL;
	};

    // Grab the ninth argument (Qone)
	pPy = PyTuple_GetItem(args,i++);

	// Parse ([integer]) Qone
	if (!parse_int_vector(pPy, Qone) == 0) {
		return NULL;
	};

    // Grab the tenth argument (reachL)
	pPy = PyTuple_GetItem(args,i++);
    double* reachL_double = new double[PyObject_Size(pPy)];

	// Parse ([double]) reachL
    if (!parse_double_list(pPy, reachL_double) == 0) {
	    return NULL;
    };

    // Grab the eleventh argument (reachU)
	pPy = PyTuple_GetItem(args,i++);
    double* reachU_double = new double[PyObject_Size(pPy)];

	// Parse ([double]) reachU
    if (!parse_double_list(pPy, reachU_double) == 0) {
	    return NULL;
    };

	// Grab the last argument (diagOnly)
	pPy = PyTuple_GetItem(args,i);
    diagOnly = PyObject_IsTrue(pPy);

    thevenin_getz(n, M, L, U, Link, LinkT, D, Pone, Qone, reachL_double, reachU_double,
            diagOnly, &v);

    vfast = package_complex_vector(v);

    return vfast;
}

// Input args: M, NITER
static PyObject *thevenin_klu_wrapper(PyObject* self, PyObject* args) {
	// Placeholder for the output and each item
	PyObject *pPy;
	PyObject *output = PyList_New(0);

    // Allocation for input args
    complex_sparse_matrix M;
    int NITER;
    // Allocation of temporary placeholders used in thevenin_klu call
    int i = 0;
    /* L, U, R and F */
    complex_sparse_matrix L, U, F;
    /* P */
    long int* P;
    long int P_size;
    /* Q */
    long int* Q;
    long int Q_size;
    /* R */
    long int* R;
    long int R_size;
    /* Rs */
    double* Rs;
    long int Rs_size;

    /* Parse the csr matrix and number of iterations arguments */
	// Grab the first argument (the csr_matrix)
	pPy = PyTuple_GetItem(args,i);

	// Parse complex_sparse_matrix "M"
	if (!parse_complex_sparse_matrix(pPy, M) == 0) {
		return NULL;
	};
	// Increment current argument counter
	i++;

	// Grab the second argument (the number of iterations)
	pPy = PyTuple_GetItem(args,i);

	// Parse (integer) number of iterations
	if (!parse_integer(pPy, &NITER) == 0) {
		return NULL;
	};

    // The thevinin calls
    thevenin_klu(M, NITER, L, U, F, &P, &P_size, &Q, &Q_size, &R, &R_size, &Rs, &Rs_size);

    // Output: 'L', 'U', 'P', 'Q', 'R', 'Rs', F'

	// Package the 'L' matrix as a Python Object
	PyObject* L_python = package_complex_sparse_matrix(L);
	// Append the result to the output list, which will be returned by the function
	PyList_Append(output, L_python);

	// Package the 'U' matrix as a Python Object
	PyObject* U_python = package_complex_sparse_matrix(U);
	// Append the result to the output list, which will be returned by the function
	PyList_Append(output, U_python);

	// Package the 'P' vector as a Python Object
    PyObject* P_python = package_long_int_list(P, P_size);
	// Append the result to the output list, which will be returned by the function
	PyList_Append(output, P_python);

	// Package the 'Q' vector as a Python Object
    PyObject* Q_python = package_long_int_list(Q, Q_size);
	// Append the result to the output list, which will be returned by the function
	PyList_Append(output, Q_python);

	// Package the 'R' vector as a Python Object
	PyObject* R_python = package_long_int_list(R, R_size);
	// Append the result to the output list, which will be returned by the function
	PyList_Append(output, R_python);

	// Package the 'Rs' vector as a Python Object
	PyObject* Rs_python = package_double_list(Rs, Rs_size);
	// Append the result to the output list, which will be returned by the function
	PyList_Append(output, Rs_python);

	// Package the 'F' matrix as a Python Object
	PyObject* F_python = package_complex_sparse_matrix(F);
	// Append the result to the output list, which will be returned by the function
    PyList_Append(output, F_python);

    return output;
}


// Input args: M, NITER
static PyObject *thevenin_klu_parkronred_wrapper(PyObject* self, PyObject* args) {
	// Placeholder for the output and each item
	PyObject *pPy;
	PyObject *output = PyList_New(0);

    // Allocation for input args
    complex_sparse_matrix A, M;
    real_matrix ops_in;
    int NITER;
    // Allocation of temporary placeholders used in thevenin_klu call
    int i = 0;
    /* L, U, R and F */
    complex_sparse_matrix L, U, F;
    /* P */
    long int* P;
    long int P_size;
    /* Q */
    long int* Q;
    long int Q_size;
    /* R */
    long int* R;
    long int R_size;
    /* Rs */
    double* Rs;
    long int Rs_size;


	// Grab the first argument (the csr_matrix A)
	pPy = PyTuple_GetItem(args,i);

	// Parse complex_sparse_matrix "A"
	if (!parse_complex_sparse_matrix(pPy, A) == 0) {
		return NULL;
	};
	// Increment current argument counter
	i++;

	// Grab the second argument (the csr_matrix M)
	pPy = PyTuple_GetItem(args,i);

	// Parse complex_sparse_matrix "M"
	if (!parse_complex_sparse_matrix(pPy, M) == 0) {
		return NULL;
	};
	// Increment current argument counter
	i++;

	// Grab the third argument (the eOps)
	pPy = PyTuple_GetItem(args,i);

	// Parse real_matrix "ops_in"
	if (!parse_real_matrix_struct(pPy, ops_in) == 0) {
		return NULL;
	};
	// Increment current argument counter
	i++;

	// Grab the last argument (the number of iterations)
	pPy = PyTuple_GetItem(args,i);

	// Parse (integer) number of iterations
	if (!parse_integer(pPy, &NITER) == 0) {
		return NULL;
	};

    // The thevinin calls
    thevenin_klu_parkronred(A, M, ops_in, NITER, L, U, F, &P, &P_size, &Q, &Q_size, &R, &R_size,
            &Rs, &Rs_size);

    // Output: 'L', 'U', 'P', 'Q', 'R', 'Rs', F'

	// Package the 'L' matrix as a Python Object
	PyObject* L_python = package_complex_sparse_matrix(L);
	// Append the result to the output list, which will be returned by the function
	PyList_Append(output, L_python);

	// Package the 'U' matrix as a Python Object
	PyObject* U_python = package_complex_sparse_matrix(U);
	// Append the result to the output list, which will be returned by the function
	PyList_Append(output, U_python);

	// Package the 'P' vector as a Python Object
    PyObject* P_python = package_long_int_list(P, P_size);
	// Append the result to the output list, which will be returned by the function
	PyList_Append(output, P_python);

	// Package the 'Q' vector as a Python Object
    PyObject* Q_python = package_long_int_list(Q, Q_size);
	// Append the result to the output list, which will be returned by the function
	PyList_Append(output, Q_python);

	// Package the 'R' vector as a Python Object
	PyObject* R_python = package_long_int_list(R, R_size);
	// Append the result to the output list, which will be returned by the function
	PyList_Append(output, R_python);

	// Package the 'Rs' vector as a Python Object
	PyObject* Rs_python = package_double_list(Rs, Rs_size);
	// Append the result to the output list, which will be returned by the function
	PyList_Append(output, Rs_python);

	// Package the 'F' matrix as a Python Object
	PyObject* F_python = package_complex_sparse_matrix(F);
	// Append the result to the output list, which will be returned by the function
    PyList_Append(output, F_python);

    return output;
}

// Input args: n, M, L, Link, Pone
static PyObject *thevenin_reach_wrapper(PyObject* self, PyObject* args) {
	// Placeholder for each item
	PyObject *pPy;
    int i = 0;
    // Allocation for input args
    int n, M;
    complex_sparse_matrix L, Link;
    std::vector<int> Pone;
    // Allocation of temporary placeholder used in thevenin_reach call
    std::vector<double> outReach;

    /* Parsing the input arguments */
	// Grab the first argument (int n)
	pPy = PyTuple_GetItem(args,i);

	// Parse (integer) number of iterations
	if (!parse_integer(pPy, &n) == 0) {
		return NULL;
	};
	// Increment current argument counter
	i++;

	// Grab the second argument (int M)
	pPy = PyTuple_GetItem(args,i);

	// Parse (integer) number of iterations
	if (!parse_integer(pPy, &M) == 0) {
		return NULL;
	};
	// Increment current argument counter
	i++;

	// Grab the third argument (csr_matrix L)
	pPy = PyTuple_GetItem(args,i);

	// Parse (csr_matrix) L
	if (!parse_complex_sparse_matrix(pPy, L) == 0) {
		return NULL;
	};
	// Increment current argument counter
	i++;

	// Grab the fourth argument (csr_matrix Link)
	pPy = PyTuple_GetItem(args,i);

	// Parse (csr_matrix) Link
	if (!parse_complex_sparse_matrix(pPy, Link) == 0) {
		return NULL;
	};
	// Increment current argument counter
	i++;

	// Grab the first argument ([int] Pone)
	pPy = PyTuple_GetItem(args,i);

	// Parse ([integer]) Pone
	if (!parse_int_vector(pPy, Pone) == 0) {
		return NULL;
	};

    /* The thevenin_reach call */
    thevenin_reach(n, M, L, Link, Pone, &outReach);

	// Package the 'outReach' matrix as a Python Object
	PyObject* Py_outReach = package_double_matrix(outReach, n+1, M);

    return Py_outReach;
}


// This is where all callable methods of the module are defined
static PyMethodDef Pythevenin_methods[] = {
    {"thevenin_getz", thevenin_getz_wrapper, METH_VARARGS, "Computes thevenin impedances"},
    {"thevenin_klu", thevenin_klu_wrapper, METH_VARARGS, "Performs LU factorization using KLU"},
    {"thevenin_klu_parkronred", thevenin_klu_parkronred_wrapper, METH_VARARGS, "Performs LU factorization using KLU, with OMP optimisation utilising parallel for-loops"},
    {"thevenin_reach", thevenin_reach_wrapper, METH_VARARGS, "Computes thevenin reach graph"},
    {NULL, NULL, 0, NULL}
};

static struct PyModuleDef pythevenin_definition = {
    PyModuleDef_HEAD_INIT,
    "thevenin",
    THEVENIN_KLU_DESCRIPTION,
    -1,
    Pythevenin_methods
};

// Creating the module named "pykronred"
PyMODINIT_FUNC PyInit_pythevenin(void) {
  Py_Initialize();
  PyObject *m = PyModule_Create(&pythevenin_definition);

  return m;
}
