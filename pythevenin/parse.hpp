/* File containing definitions for all methods used
 * for parsing input expected of the user currently using
 * the pykronred python extension.
 */


// Make sure the header is only included once
#ifndef SPARSE
#define SPARSE
#include "thevenin/sparse_definition.hpp"
#endif // SPARSE
#ifndef REALMATRIX
#define REALMATRIX
#include "thevenin/real_matrix.hpp"
#endif // REALMATRIX
// Used for making/using python objects in c++
#include <Python.h>
#include <vector>
#include <complex>

// Parse a complex_sparse_matrix from python object (Mpy) and "save" it in
// the matrix M
int parse_complex_sparse_matrix(PyObject* MPy, complex_sparse_matrix& M);
// Parse a list of doubles
int parse_double_list(PyObject* dListPy, double* dList);
// Parse a list of integer "into" a vector
int parse_int_vector(PyObject* vecPy, std::vector<int>& vec);
// Parse an integer
int parse_integer(PyObject* nPy, int* n);
// Parse a list of long ints
int parse_long_list(PyObject* lListPy, long int* lList);
// Parse a "real matrix" struct
int parse_real_matrix_struct(PyObject* fListPy, real_matrix& rM);
