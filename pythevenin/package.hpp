/* File containing definitions of all methods used for packaging
 * C++ objects as python objects. */

// Make sure the header is only included once
#ifndef SPARSE
#define SPARSE
#include "thevenin/sparse_definition.hpp"
#endif // SPARSE
#include <Python.h>
#include <vector>

PyObject* package_complex_sparse_matrix(complex_sparse_matrix& M);
PyObject* package_double_list(double* dList, int size);
PyObject* package_double_vector(std::vector<double> v);
PyObject* package_long_int_list(long int* dList, int size);
PyObject* package_double_matrix(std::vector<double>& vec, int n, int m);
PyObject* package_complex_vector(std::vector<std::complex<double>> v);
