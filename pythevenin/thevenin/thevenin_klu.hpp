/*
 * This file is part of networkred.
 *
 * Copyright (C) 2012/2017, Technical University of Denmark/University of Copenhagen
 * https://bitbucket.org/stefansommer/networkred
 *
 * networkred is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * networkred is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with networkred.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "networkred_common.h"
#ifndef SPARSE
#define SPARSE
#include "sparse_definition.hpp"
#endif // SPARSE
#include "klu.h"
#include "klu_cholmod.h"
#include <omp.h>

#define Long SuiteSparse_long

void thevenin_klu(complex_sparse_matrix& M, int NITER, complex_sparse_matrix& L,
        complex_sparse_matrix& U, complex_sparse_matrix& Off, long int** P, long int* P_size,
        long int** Q, long int* Q_size, long int** R, long int* R_size, double** Rs,
        long int* Rs_size);
