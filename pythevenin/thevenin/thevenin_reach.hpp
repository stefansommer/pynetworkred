#include "networkred_common.h"
#ifndef SPARSE
#define SPARSE
#include "sparse_definition.hpp"
#endif // SPARSE
#define CS_LONG
#define CS_COMPLEX
#include "cs.h"
#define Long SuiteSparse_long
#include "ssparse.h"
#include <omp.h>
#include <vector>

void thevenin_reach(int n, int M, complex_sparse_matrix& L, complex_sparse_matrix& Link,
        std::vector<int> Pone, std::vector<double>* outReach);
