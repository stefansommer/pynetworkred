/*
 *  This file is part of pynetworkred.
 *
 *  Copyright (C) 2019, University of Copenhagen
 *  https://bitbucket.org/stefansommer/pynetworkred
 *  
 *  pynetworkred is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  pynetworkred is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with networkred.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "thevenin_klu.hpp"

// Output: 'LU', outTime
void thevenin_klu(complex_sparse_matrix& M, int NITER, complex_sparse_matrix& L,
        complex_sparse_matrix& U, complex_sparse_matrix& Off, long int** P, long int* P_size,
        long int** Q, long int* Q_size, long int** R, long int* R_size, double** Rs,
        long int* Rs_size) {
    klu_l_symbolic *Symbolic;
    klu_l_numeric *Numeric;
    klu_l_common Common;
    Long symmetric;


    Long n = M.rows;
    // Set KLU options
    klu_l_defaults (&Common);

    if (Common.ordering < 0 || Common.ordering > 4)
    {
        printf("invalid ordering option\n") ;
        return;
    }

    /* ordering option 3,4 becomes KLU option 3, with symmetric 0 or 1 */
    symmetric = (Common.ordering == 4) ;
    if (symmetric) Common.ordering = 3 ;
    Common.user_order = klu_l_cholmod ;
    Common.user_data = &symmetric ;
    Common.tol = 0; // no pivoting
    Common.scale = -1; // no scaling

    // Factorize

    Symbolic = klu_l_analyze (n, M.p, M.i, &Common) ;
    if (Symbolic == (klu_l_symbolic *) NULL)
    {
        printf("klu symbolic analysis failed\n");
        return;
    }

    // Factorization
    Symbolic = klu_l_analyze(M.rows, M.p, M.i, &Common);

    for (int i=0; i<NITER; i++) {
        Numeric = klu_zl_factor(M.p, M.i, (double*)M.v, Symbolic, &Common);
    }

    if (Common.status != KLU_OK)
    {
        printf("klu numeric factorization failed\n");
        return;
    }

    // LU = klu (A) usage; extract factorization

    /* sort the row indices in each column of L and U (only needed in MATLAB) */
    klu_zl_sort (Symbolic, Numeric, &Common) ;

    /* Allocating memory for the complex sparse matrices L, U, Off */

    /* L */
    L.rows = Numeric->n;
    L.nz = Numeric->lnz;
    L.p = new long int[Numeric->n+1];
    L.i = new long int[Numeric->lnz];
    L.x = new double[Numeric->lnz];
    L.z = new double[Numeric->lnz];


    /* U */
    U.rows = Numeric->n;
    U.nz = Numeric->unz;
    U.p = new long int[Numeric->n+1];
    U.i = new long int[Numeric->unz];
    U.x = new double[Numeric->unz];
    U.z = new double[Numeric->unz];

    /* off-diagonal entries */
    Off.rows = Numeric->n;
    Off.nz = Numeric->nzoff;
    Off.p = new long int[Numeric->n+1];
    Off.i = new long int[Numeric->nzoff];
    Off.x = new double[Numeric->nzoff];
    Off.z = new double[Numeric->nzoff];

    /* P */
    *P = new long int[Numeric->n];
    *P_size = Numeric->n;

    /* Q */
    *Q = new long int[Numeric->n];
    *Q_size = Numeric->n;

    /* Rs */
    *Rs = new double[Numeric->n];
    *Rs_size = Numeric->n;

    /* R */
    *R = new long int[Numeric->n];
    *R_size = Numeric->n;

    // Extract from klu symbolic and numeric
    int ok = klu_zl_extract (Numeric, Symbolic, L.p, L.i, L.x, L.z, U.p, U.i, U.x, U.z,
            Off.p, Off.i, Off.x, Off.z, *P, *Q, *Rs, *R, &Common) ;

    if (ok != 1) {
        printf("klu extraction failed with error code %d\n", ok);
        return;
    }

    // Clean up

    klu_l_free_symbolic (&Symbolic, &Common) ;
    klu_l_free_numeric (&Numeric, &Common) ;
}
