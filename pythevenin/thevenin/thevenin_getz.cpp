/*
 *  This file is part of pynetworkred.
 *
 *  Copyright (C) 2019, University of Copenhagen
 *  https://bitbucket.org/stefansommer/pynetworkred
 *  
 *  pynetworkred is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  pynetworkred is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with networkred.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "thevenin_klu.hpp"

#define CS_LONG
#define CS_COMPLEX
#include "cs.h"
#define Long SuiteSparse_long
#include "ssparse.h"

#include <omp.h>
#include <fstream>
#include <string>
#include <vector>
// For printing (TODO: Remove all prints)
#include <iostream>

void thevenin_getz(int n, int M, complex_sparse_matrix& L, complex_sparse_matrix& U,
        complex_sparse_matrix& Link, complex_sparse_matrix& LinkT, complex_sparse_matrix& D,
        std::vector<int> Pone, std::vector<int> Qone, double* reachL_double,
        double* reachU_double, bool diagOnly, std::vector<std::complex<double>>* v_return) {

    // Setup data structures
    int NITER = 1;

    Long *P = (Long*)malloc(n * sizeof(Long));
    for (int i=0; i<n; i++)
        P[i] = Pone[i];
    Long *Pinv = cs_pinv(P,n);
    if (Pinv == NULL) {
        printf("pinv allocation failed\n");
        return;
    }

    Long *Q = (Long*)malloc(n * sizeof(Long));
    for (int i=0; i<n; i++)
        Q[i] = Qone[i];
    Long *Qinv = cs_pinv(Q,n);
    if (Qinv == NULL) {
        printf("qinv allocation failed\n");
        return;
    }

    CLJ_CS_TO_CS_CL(Lcs,L);
    CLJ_CS_TO_CS_CL(Ucs,U);

    cs_complex_t *B = (cs_complex_t*)malloc (Link.nz * sizeof (cs_complex_t)) ;
    Long *Bi = (Long*)malloc (Link.nz * sizeof (Long)) ;
    for (int c=0; c<M; c++) {
        for (int k=Link.p[c]; k<Link.p[c+1]; k++) {
            B[k] = Link.v[k];
            Bi[k] = Pinv[Link.i[k]];
        }
    }
    CLJ_CS_TO_CS_CL(Bcs,Link);
    Bcs.i = Bi;
    Bcs.x = B;

    Long *LinkTQi = (Long*)malloc (LinkT.nz * sizeof (Long)) ;
    for (int c=0; c<M; c++) {
        for (int k=LinkT.p[c]; k<LinkT.p[c+1]; k++) {
            LinkTQi[k] = Qinv[LinkT.i[k]];
        }
    }
    CLJ_CS_TO_CS_CL(LinkTcs,LinkT);
    LinkTcs.i = LinkTQi;


    CS_INT *reachL = (CS_INT*)malloc ((1 + n) * M * sizeof (CS_INT)) ;
    for (int i=0; i<(1+n)*M; i++)
        reachL[i] = (CS_INT)reachL_double[i];
    CS_INT *reachU = (CS_INT*)malloc ((1 + n) * M * sizeof (CS_INT)) ;
    for (int i=0; i<(1+n)*M; i++)
        reachU[i] = (CS_INT)reachU_double[i];


    // Global results, workspace, etc.

    cs_complex_t *vs = (cs_complex_t*)malloc (M * sizeof (cs_complex_t)) ;
    cs_complex_t *S = (cs_complex_t*)malloc (M * M * sizeof (cs_complex_t)) ;
    // zero
    for (int i=0; i<M; i++)
        vs[i] = 0;
    for (int i=0; i<M*M; i++)
        S[i] = 0;

    cs_complex_t *global_x = (cs_complex_t*)malloc(n * sizeof(cs_complex_t) * M);
    cs_complex_t *global_y = (cs_complex_t*)malloc(n * sizeof(cs_complex_t) * M);
    Long *global_topx = (Long*)malloc(M * sizeof(Long));
    Long *global_topy = (Long*)malloc(M * sizeof(Long));

    // zero
    for (int i=0; i<n*M; i++)
        global_x[i] = global_y[i] = 0;

    // Compute

    for (int iter=0; iter<NITER; iter++) {


#pragma omp parallel for schedule(static)
        for (int K=0; K<M; K++) {

            cs_complex_t *x = &global_x[n*K];
            cs_complex_t *y = &global_y[n*K];
            Long *topx = &global_topx[K];
            Long *topy = &global_topy[K];

            // do backwards solve
            Long *topyi = reachL+(1+n)*K;
            Long *topxi = reachU+(1+n)*K;


            *topy = ssp_cl_splsolve(&Lcs,&Bcs,K,topyi,y,NULL);
            *topx = ssp_cl_sputsolve(&Ucs,&LinkTcs,K,topxi,x,NULL);
        }


#pragma omp parallel for schedule(static)
        for (int c=0; c<M; c++) {

            int start_r, end_r;
            if (diagOnly) {
                start_r = c;
                end_r = c+1;
            } else {
                start_r = 0;
                end_r = M;
            }

            for (int r=start_r; r<end_r; r++) {
                // get data
                cs_complex_t *x = &global_x[n*r];
                cs_complex_t *y = &global_y[n*c];
                Long *topx = &global_topx[r];

                Long *topxi = reachU+(1+n)*r;
                Long *xi = topxi+1;

                // inner product
                CS_ENTRY ip = ssp_spdot(xi+*topx,x,y,n-*topx);

                // Schur complement
                cs_complex_t Urc = 0;
                for (int i=D.p[c]; i<D.p[c+1]; i++) {
                    if (D.i[i] == r) {
                        Urc = D.v[i];
                        break;
                    }
                }

                cs_complex_t Src = Urc-ip;
                // store value
                S[M*c+r] = Src;

                // Thevenin imp
                if (c == r) {
                    cs_complex_t v = cs_complex_t(1,0)/Src;
                    vs[c] = v;
                }
            }

        }

    }

    // Here, stuff should probably be moved from vs into v_return
    for (int i = 0; i < M; i++) {
        v_return->push_back(vs[i]);
    }


    // Clean up
    cs_free(Pinv);
    cs_free(Qinv);
}
