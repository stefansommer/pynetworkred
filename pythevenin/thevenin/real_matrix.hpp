/*
 * Header file defining the "complex_sparse_matrix" type, which will
 * be included by all other "kronred" src files.
 */

#include <complex>

typedef struct {
    double *x;
    int rows;
    int cols;
} real_matrix;
