
#include "networkred_common.h"
#ifndef SPARSE
#define SPARSE
#include "sparse_definition.hpp"
#endif // SPARSE
#include "klu.h"
#include "klu_cholmod.h"
#include <omp.h>

//void thevenin_getz(int n, int M, complex_sparse_matrix& L, complex_sparse_matrix& U,
//        complex_sparse_matrix& Link, complex_sparse_matrix& LinkT, complex_sparse_matrix& D,
//        std::vector<int> Pone, std::vector<int> Qone, double* reachL_double,
//        double* reachU_double, bool diagOnly, std::vector<double>* v_return);
void thevenin_getz(int n, int M, complex_sparse_matrix& L, complex_sparse_matrix& U,
        complex_sparse_matrix& Link, complex_sparse_matrix& LinkT, complex_sparse_matrix& D,
        std::vector<int> Pone, std::vector<int> Qone, double* reachL_double,
        double* reachU_double, bool diagOnly, std::vector<std::complex<double>>* v_return);
