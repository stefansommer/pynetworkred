/*
 *  This file is part of pynetworkred.
 *
 *  Copyright (C) 2019, University of Copenhagen
 *  https://bitbucket.org/stefansommer/pynetworkred
 *  
 *  pynetworkred is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  pynetworkred is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with networkred.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "thevenin_reach.hpp"
#include <iostream>

//void thevenin_reach(int n, int M, complex_sparse_matrix& L, complex_sparse_matrix& Link,
//        std::vector<int> Pone, std::vector<double>* outReach) {
void thevenin_reach(int n, int M, complex_sparse_matrix& L, complex_sparse_matrix& Link,
        std::vector<int> Pone, std::vector<double>* outReach) {

    // Setup data structures

    CLJ_CS_TO_CS_CL(Lcs,L);

    Long *P = (Long*)malloc(n * sizeof(Long));
    for (int i=0; i<n; i++)
        P[i] = Pone[i];
    Long *Pinv = cs_pinv(P,n);
    if (Pinv == NULL) {
        printf("pinv allocation failed\n");
        return;
    }
    //for (int c=0; c<M; c++) {
    //    for (int k=Lcs.p[c]; k<Lcs.p[c+1]; k++) {
    //        std::cout << Lcs.x[Lcs.i[k]] << "\n";
    //    }
    //}

    cs_complex_t *B = (cs_complex_t*)malloc (Link.nz * sizeof (cs_complex_t)) ;
    Long *Bi = (Long*)malloc (Link.nz * sizeof (Long)) ;
    for (int c=0; c<M; c++) {
        for (int k=Link.p[c]; k<Link.p[c+1]; k++) {
            B[k] = cs_complex_t(Link.x[k],Link.z[k]);
            Bi[k] = Pinv[Link.i[k]];
            //std::cout << Link.i[k] << "\n";
            //std::cout << B[k] << "\n";
        }
    }
    CLJ_CS_TO_CS_CL(Bcs,Link);
    Bcs.i = Bi;
    Bcs.x = B;

    // Global results, workspace, etc.

    CS_INT *reach = (CS_INT*)malloc ((1 + 2*n) * M * sizeof (CS_INT)) ;
    for (int i=0; i<(1+2*n)*M; i++)
        reach[i] = 0;

    // Compute

    for (int k=0; k<M; k++) {
        // Here, some Bus Error occur (trying to access memory it can't)
        CS_INT top = cs_reach (&Lcs, &Bcs, k, reach+(1+2*n)*k+1, NULL);
        reach[(1+2*n)*k] = top;
        //std::cout << top << "\n";
    }

    // Output

    // Should create a M x n+1
    for (int j=0; j<n+1; j++)
        for (int k=0; k<M; k++)
            //outReach[n*k+j] = (double)reach[(1+2*n)*k+j]; - Original source code
            outReach->push_back((double)reach[(1+2*n)*k+j]);

    cs_free(Pinv);
}
