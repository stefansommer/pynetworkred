#
# This file is part of pynetworkred.
#
# Copyright (C) 2019, University of Copenhagen
# https://bitbucket.org/stefansommer/pynetworkred
# 
# pynetworkred is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# pynetworkred is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with networkred.  If not, see <http://www.gnu.org/licenses/>.
# 

from distutils.core import setup, Extension
import os

os.environ["CC"] = "g++"
os.environ["CXX"] = "g++"
SUITESPARSE_FOLDER = os.environ["SUITE_SPARSE"]
SSPARSE_FOLDER = SUITESPARSE_FOLDER + '/ssparse'

thevenin_module = Extension(
    'pythevenin',
    sources=['thevenin_module.cpp',
             'thevenin/thevenin_klu.cpp',
             'thevenin/thevenin_reach.cpp',
             'thevenin/thevenin_getz.cpp',
             'kronred/kronred.cpp',
             'kronred/kronred_factor.cpp',
             'kronred/kronred_symbolic.cpp',
             'kronred/kronred_common.cpp',
             'parse.cpp',
             'package.cpp'],
    language='C++',
    extra_compile_args=['-std=c++11', '-libstdc++',
        '-lcholmod',
        '-I' + SUITESPARSE_FOLDER + '/SuiteSparse_config',
        '-I' + SUITESPARSE_FOLDER + '/CXSparse/Include',
        '-L' + SUITESPARSE_FOLDER + '/KLU/User',
        '-L' + SUITESPARSE_FOLDER + '/KLU/Include',
        '-L' + SUITESPARSE_FOLDER + '/CHOLMOD/Lib',
        '-I' + SUITESPARSE_FOLDER + '/KLU/User',
        '-I' + SUITESPARSE_FOLDER + '/KLU/Include',
        '-I' + SUITESPARSE_FOLDER + '/AMD/Include',
        '-I' + SUITESPARSE_FOLDER + '/COLAMD/Include',
        '-I' + SUITESPARSE_FOLDER + '/BTF/Include',
        '-L' + SUITESPARSE_FOLDER + '/lib',
        '-I' + SSPARSE_FOLDER + '/include',
        '-fopenmp'], )

setup(
    name='pythevenin',
    version='0.1.0',
    description='Module containing thevenin helper methods',
    ext_modules=[thevenin_module], )
