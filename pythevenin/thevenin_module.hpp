/* Header file containing method descriptions, etc. */
#define THEVENIN_KLU_DESCRIPTION "A Python module containing methods for computing thevenin impedances and performing various factorizations"
