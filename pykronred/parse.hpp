/* File containing definitions for all methods used
 * for parsing input expected of the user currently using
 * the pykronred python extension.
 */


// Make sure the header is only included once
#ifndef SPARSE
#define SPARSE
#include "kronred/sparse_definition.hpp"
#endif // SPARSE
// Used for making/using python objects in c++
#include <Python.h>
#include "kronred/kronred.hpp"
#include <vector>

// Parse a complex_sparse_matrix from python object (Mpy) and "save" it in
// the matrix M
int parse_complex_sparse_matrix(PyObject* MPy, complex_sparse_matrix& M);
// Parse encoded ops into object eOps
int parse_encoded_ops(PyObject* eOpsPy, EncodedOps* eOps);
// Parse a vector of integers (indices)
int parse_vector_of_integers(PyObject* lPy, std::vector<int>* l);
// Parse an integer
int parse_integer(PyObject* nPy, int* n);
// Parse a list of doubles
int parse_double_list(PyObject* dListPy, double* dList);
// Parse a list of long ints
int parse_long_list(PyObject* lListPy, long int* lList);
// Parse encodedOps
int parse_encodedOps(PyObject* eOpsPy, EncodedOps& eOps);
// Parse a list of integer "into" a vector
int parse_int_vector(PyObject* vecPy, std::vector<int>& vec);
