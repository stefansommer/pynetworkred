/* 
 * File containing the implementations of all methods
 * from "package.hpp" used for packaging C++ objects
 * "into" Python objects.
 *
 *  This file is part of pynetworkred.
 *
 *  Copyright (C) 2019, University of Copenhagen
 *  https://bitbucket.org/stefansommer/pynetworkred
 *  
 *  pynetworkred is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  pynetworkred is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with networkred.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "package.hpp"

PyObject* package_complex_sparse_matrix(complex_sparse_matrix& M) {
	/* Object placeholders - the final object will be a list containing all fields as they
	 * occur in the complex_sparse_matrix definition */

	// Placeholder which will be used for storing the "csr_matrix"
	PyObject* csr_matrix;

	// Parsing the lists
    PyObject* indptr = PyList_New(0);
	PyObject* indices = PyList_New(0);
    PyObject* data = PyList_New(0);
    PyObject* int_placeholder;
	// indptr
	for (int i = 0; i < M.rows+1; i++) {
        int_placeholder = Py_BuildValue("i", M.p[i]);
		PyList_Append(indptr, int_placeholder);
	}
    // indices
	for (int i = 0; i < M.nz; i++) {
        int_placeholder = Py_BuildValue("i", M.i[i]);
		PyList_Append(indices, int_placeholder);
	}

    // This part is necessary to package the complex part of the sparse matrix
	Py_complex complexPlaceholder; // struct { double real; double imag } Py_complex;
	PyObject* complexPy;
	for (int i = 0; i <  M.nz; i++) {
		complexPlaceholder.real = M.x[i];
		complexPlaceholder.imag = M.z[i];
		complexPy = Py_BuildValue("D", &complexPlaceholder);

		PyList_Append(data, complexPy);
	}

    // Import the scipy.sparse module
	PyObject* scipy_sparse = PyImport_ImportModule("scipy.sparse");
    // Make the indices, indptr, data lists into numpy arrays
	PyObject* numpy = PyImport_ImportModule("numpy");
    // Initialize the lists as numpy arrays
    indices = PyObject_CallMethod(numpy, "array", "O", indices);
    indptr = PyObject_CallMethod(numpy, "array", "O", indptr);
    data = PyObject_CallMethod(numpy, "array", "O", data);

    // The shape is needed to construct the scipy.sparse.csr_matrix object
    PyObject* shape = Py_BuildValue("ii", M.rows, M.rows);
	if (shape == NULL) {
		return NULL;
	}

    // Creates a tuple containgin (data, indices, indptr) as a python object
	PyObject* datatuple = Py_BuildValue("OOO", data, indices, indptr);
	if (datatuple == NULL) {
		return NULL;
	}

    // Construct the "csr_matrix" using the "scipy.sparse" library
	csr_matrix = PyObject_CallMethod(scipy_sparse, "csr_matrix", "OO", datatuple, shape);
	if (csr_matrix == NULL) {
		return NULL;
	}

	return csr_matrix;
}

PyObject* package_double_list(double* dList, int size) {
	/* Placeholder for the new python list */
	PyObject* output = PyList_New(0);

	for (int i = 0; i < size; i++) {
		PyList_Append(output, PyFloat_FromDouble(dList[i]));
	}

	return output;

}
