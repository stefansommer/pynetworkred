#
# This file is part of pynetworkred.
#
# Copyright (C) 2019, University of Copenhagen
# https://bitbucket.org/stefansommer/pynetworkred
# 
# pynetworkred is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# pynetworkred is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with networkred.  If not, see <http://www.gnu.org/licenses/>.
# 

from distutils.core import setup, Extension
import os

os.environ["CC"] = "g++"
os.environ["CXX"] = "g++"
SUITESPARSE_FOLDER = os.environ["SUITE_SPARSE"]

kronred_module = Extension(
    'pykronred',
    sources=['kronred_module.cpp',
             'kronred/kronred.cpp',
             'kronred/kronred_factor.cpp',
             'kronred/kronred_symbolic.cpp',
             'kronred/kronred_common.cpp',
             'parse.cpp',
             'package.cpp'],
    language='C++',
    extra_compile_args=['-std=c++11', '-libstdc++',
        '-I' + SUITESPARSE_FOLDER + '/CSparse/Include'], )

setup(
    name='pykronred',
    version='0.1.0',
    description='Module for performing kron reduction',
    ext_modules=[kronred_module], )
