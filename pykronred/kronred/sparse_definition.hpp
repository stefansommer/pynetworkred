/*
 * Header file defining the "complex_sparse_matrix" type, which will
 * be included by all other "kronred" src files.
 */

#include <complex>

typedef struct {
    long int *p;
    long int *i;
    double *x;
    double *z;
    int rows;
    int cols;
    int nz;
	std::complex<double> *v; // zipped copy of x and z - is this even needed?
} complex_sparse_matrix;
