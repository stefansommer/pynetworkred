/*
 *  This file is part of pynetworkred.
 *
 *  Copyright (C) 2019, University of Copenhagen
 *  https://bitbucket.org/stefansommer/pynetworkred
 *  
 *  pynetworkred is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  pynetworkred is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with networkred.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "kronred.hpp"
#include "kronred_common.hpp"

//#include "klu_internal.h"

#include "kronred_factor.hpp"
#include <cstring>
#include <omp.h>

void kronred(complex_sparse_matrix& M, EncodedOps& eOps, std::vector<int>& index,
        complex_sparse_matrix& Mkr) {

    double* Mx = new double[M.nz+1];
    double* Mz = new double[M.nz+1];

    // Allocating "Mkr"
    Mkr.p = new long int[M.rows+1];
    Mkr.i = new long int[M.nz+1];
    Mkr.x = new double[M.nz+1];
    Mkr.z = new double[M.nz+1];

    // Factorize
    std::memcpy(Mx,M.x,M.nz*sizeof(double));
    std::memcpy(Mz,M.z,M.nz*sizeof(double));
    kronred_factor(Mx, Mz, eOps[0].data(), eOps.size());

    // Extract Mkr
    kronred_extract(M, Mx, Mz, index, Mkr);
}
