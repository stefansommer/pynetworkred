/*
 *  This file is part of pynetworkred.
 *
 *  Copyright (C) 2019, University of Copenhagen
 *  https://bitbucket.org/stefansommer/pynetworkred
 *  
 *  pynetworkred is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  pynetworkred is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with networkred.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <Python.h>
#include "kronred/kronred_symbolic.hpp"
#include <iostream>
// Packages for assisting parsing and packaging PyObjects
#include "parse.hpp"
#include "package.hpp"

static PyObject *kronred_symbolic_wrapper(PyObject *self, PyObject *args) {
    // PyObject which will be returned by the wrapper
	PyObject *output = PyList_New(0);

	/* Allocating variables which arguments will be parsed into */
	complex_sparse_matrix M, Msymbolic;
	int Nnvc;
	int cutDegree;
	int Nsweeps;
	// What does the two booleans mean?
	int bool1;
	int bool2;
	int Nkr;
    std::vector<int> index;
    EncodedOps eOps1, eOps2;

	// Argument counter
	Py_ssize_t i = 0;
	// Used for iterating through the arguments
    PyObject *pPy;

	// Grab the first argument (the csr_matrix)
	pPy = PyTuple_GetItem(args,i);

	// Parse complex_sparse_matrix "M"
	if (!parse_complex_sparse_matrix(pPy, M) == 0) {
		return NULL;
	};
	// Increment current argument counter
	i++;

	// Parse the integer NNVC
	pPy = PyTuple_GetItem(args,i);
	if (!parse_integer(pPy, &Nnvc) == 0) {
		return NULL;
	}
	// Increment current argument counter
	i++;

	// Parse the integer cutDegree
	pPy = PyTuple_GetItem(args,i);
	if (!parse_integer(pPy, &cutDegree) == 0) {
		return NULL;
	}
	// Increment current argument counter
	i++;

	// Parse the integer Nsweeps
	pPy = PyTuple_GetItem(args,i);
	if (!parse_integer(pPy, &Nsweeps) == 0) {
		return NULL;
	}
	// Increment current argument counter
	i++;

	// Parse the Boolean bool1 (as an integer)
	pPy = PyTuple_GetItem(args,i);
	if (!parse_integer(pPy, &bool1) == 0) {
		return NULL;
	}
	// Increment current argument counter
	i++;

	// Parse the Boolean bool2 (as an integer)
	pPy = PyTuple_GetItem(args,i);
	if (!parse_integer(pPy, &bool2) == 0) {
		return NULL;
	}

	// Perform the "kronred_symbolic" step
	kronred_symbolic(M, Nnvc, cutDegree, Nsweeps, bool1, bool2, Msymbolic, Nkr, index, eOps1, eOps2);

	// Package the symbolic matrix as Python Objects
	PyObject* csr_matrix = package_complex_sparse_matrix(Msymbolic);

	// Append the result to the output list, which will be returned by the function
	PyList_Append(output, csr_matrix);

    // Initializing the rest of the output as python objects (lists)
    PyObject *index_py = PyList_New(0);
    PyObject *eOps1_py = PyList_New(0);
    PyObject *eOps2_py = PyList_New(0);
	// Nkr
	PyList_Append(output, PyLong_FromLong(Nkr));
	// index
	for (int i = 0; i < (int)index.size(); i++) {
		PyList_Append(index_py, PyLong_FromLong(index[i]));
	}
	PyList_Append(output, index_py);
	// eOps1
	for (int i = 0; i < (int)eOps1.size(); i++) {
		PyObject *tmpEops = PyList_New(0);
		// Set all the correct "flags"
		for (int j = 0; j < 4; j++) {
			PyList_Append(tmpEops, PyLong_FromLong(eOps1[i][j]));
		}
		PyList_Append(eOps1_py, tmpEops);
	}
	PyList_Append(output, eOps1_py);
	// eOps2
	for (int i = 0; i < (int)eOps2.size(); i++) {
		PyObject *tmpEops = PyList_New(0);
		// Set all the correct "flags"
		for (int j = 0; j < 4; j++) {
			PyList_Append(tmpEops, PyLong_FromLong(eOps2[i][j]));
		}
		PyList_Append(eOps2_py, tmpEops);
	}
	PyList_Append(output, eOps2_py);

    return output;
}

static PyObject *kronred_extract_wrapper(PyObject *self, PyObject *args) {
	// Placeholders for the input arguments (and Mkr)
	complex_sparse_matrix Msymbolic;
	std::vector<int> index;

	// Argument counter
	Py_ssize_t i = 0;
	// Used for iterating through the arguments
    PyObject *pPy;

	// Grab the first argument (the csr_matrix)
	pPy = PyTuple_GetItem(args,i);

	// Parse complex_sparse_matrix "Msymbolic"
	if (!parse_complex_sparse_matrix(pPy, Msymbolic) == 0) {
		return NULL;
	};
	// Increment current argument counter
	i++;

	// Grab the first argument (the Mxtemp list)
	pPy = PyTuple_GetItem(args,i);

	// Parsing Mxtemp
	double* Mxtemp = new double[PyList_Size(pPy)];
	if (!parse_double_list(pPy, Mxtemp) == 0) {
		return NULL;
	}
	i++;

	// Parsing Mztemp
	pPy = PyTuple_GetItem(args,i);
	double* Mztemp = new double[PyList_Size(pPy)];
	if (!parse_double_list(pPy, Mztemp) == 0) {
		return NULL;
	}
	i++;

	// Parsing index
	pPy = PyTuple_GetItem(args,i);
	long int* intList = new long int[PyList_Size(pPy)];
	if (!parse_long_list(pPy, intList) == 0) {
		return NULL;
	}
	// Place the list elements in the vector "index"
	for (int j = 0; j < PyList_Size(pPy); j++) {
		index.push_back(intList[j]);
	}
	i++;

    // Parsing the "dimensionality" of Mkr, which is needed to pre-allocate memory
	pPy = PyTuple_GetItem(args,i);
    int Mn;
    if (!PyArg_Parse(pPy, "i", &Mn)) {
        return NULL;
    }

    // Allocate "Mkr", which will hold the reduction
	complex_sparse_matrix Mkr;

    Mkr.p = new long int[Mn+1];
    Mkr.i = new long int[Mn*Mn];
    Mkr.x = new double[Mn*Mn];
    Mkr.z = new double[Mn*Mn];

	kronred_extract(Msymbolic, Mxtemp, Mztemp, index, Mkr);

    PyObject* csr_matrix = package_complex_sparse_matrix(Mkr);

	return csr_matrix;
}

static PyObject *kronred_wrapper(PyObject *self, PyObject *args) {
	// Input args: Mxtemp, Mztemp, eOps1
	// Placeholder for the output and each item
	PyObject *pPy;
	PyObject *output = PyList_New(0);
    EncodedOps* eOps = new std::vector<EncodedOp>;
	std::vector<int> index;
	complex_sparse_matrix M, Mkr;
    // Iterator through the arguments
    int i = 0;

	// Grab the first argument (the csr_matrix)
	pPy = PyTuple_GetItem(args,i);

	// Parse complex_sparse_matrix "M"
	if (!parse_complex_sparse_matrix(pPy, M) == 0) {
		return NULL;
	};
    i++;

	// Grab the second argument (the EncodedOps)
	pPy = PyTuple_GetItem(args,i);
	// Parsing eOps
	if (!parse_encodedOps(pPy, *eOps) == 0) {
		return NULL;
	};
    i++;

	// Parsing index
	pPy = PyTuple_GetItem(args,i);
	long int* intList = new long int[PyList_Size(pPy)];
	if (!parse_long_list(pPy, intList) == 0) {
		return NULL;
	}
	// Place the list elements in the vector "index"
	for (int j = 0; j < PyList_Size(pPy); j++) {
		index.push_back(intList[j]);
	}

	// Performing the kronred call
	kronred(M, *eOps, index, Mkr);

	PyObject* MkrPy = package_complex_sparse_matrix(Mkr);

	PyList_Append(output, MkrPy);

	return output;
}

// This is where all callable methods of the module are defined
static PyMethodDef Pykronred_methods[] = {
    {"kronred_symbolic", kronred_symbolic_wrapper, METH_VARARGS, "Performs Kron Symbolic Reduction"},
    {"kronred", kronred_wrapper, METH_VARARGS, "Performs Kron Reduction"},
    {"kronred_extract", kronred_extract_wrapper, METH_VARARGS, "Performs Kron Reduction"},
    {NULL, NULL, 0, NULL}
};

static struct PyModuleDef pykronred_definition = {
    PyModuleDef_HEAD_INIT,
    "kronred",
    "A Python module containing methods for performing Kron Reduction",
    -1,
    Pykronred_methods
};

// Creating the module named "pykronred"
PyMODINIT_FUNC PyInit_pykronred(void) {
  Py_Initialize();
  PyObject *m = PyModule_Create(&pykronred_definition);

  return m;
}
