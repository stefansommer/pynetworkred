/* File containing definitions of all methods used for packaging
 * C++ objects as python objects. */

// Make sure the header is only included once
#ifndef SPARSE
#define SPARSE
#include "kronred/sparse_definition.hpp"
#endif // SPARSE
#include <Python.h>

PyObject* package_complex_sparse_matrix(complex_sparse_matrix& M);
PyObject* package_double_list(double* dList, int size);
