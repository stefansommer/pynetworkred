/* 
 * File containing the implementations of all methods
 * from "parse.hpp" used for parsing python objects
 * "into" c++ objects/types.
 *
 *  This file is part of pynetworkred.
 *
 *  Copyright (C) 2019, University of Copenhagen
 *  https://bitbucket.org/stefansommer/pynetworkred
 *  
 *  pynetworkred is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  pynetworkred is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with networkred.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <complex>
#include "parse.hpp"
#include <iostream>
#include <vector>

int parse_integer(PyObject* pInt, int* i) {
	if (!PyArg_Parse(pInt, "l", i)) {
		return -1;
	}
	return 0;
}

int parse_double_list(PyObject* dListPy, double* dList) {
	/* Placeholders for objects for each item in the dListPy
	 * and the size of the list */
	PyObject* pPyItem;
	int size = PyList_Size(dListPy);
	double placeholder;

	// Parse the elements of the list into the newly initialized list
	for (int i = 0; i < size; i++) {
		pPyItem = PyList_GetItem(dListPy, i);
		if (!PyArg_Parse(pPyItem, "d", &placeholder)) {
			return -1;
		}
		dList[i] = placeholder;
	}

	return 0;
}

int parse_long_list(PyObject* lListPy, long int* lList) {
	/* Placeholders for objects for each item in the lListPy
	 * and the size of the list */
	PyObject* pPyItem;
	int size = PyList_Size(lListPy);
	long int placeholder;

	// Parse the elements of the list into the newly initialized list
	for (int i = 0; i < size; i++) {
		pPyItem = PyList_GetItem(lListPy, i);
		if (!PyArg_Parse(pPyItem, "l", &placeholder)) {
			return -1;
		}
		lList[i] = placeholder;
	}

	return 0;
}

int parse_int_vector(PyObject* vecPy, std::vector<int>* vec) {
	/* Placeholders for objects for each item in the lListPy
	 * and the size of the list */
	PyObject* pPyItem;
	int size = PyList_Size(vecPy);
	long int placeholder;

	// Parse the elements of the list into the newly initialized list
	for (int i = 0; i < size; i++) {
		pPyItem = PyList_GetItem(vecPy, i);
		if (!PyArg_Parse(pPyItem, "l", &placeholder)) {
			return -1;
		}
		vec->push_back(placeholder);
	}

	return 0;
}

int parse_complex_sparse_matrix(PyObject* MPy, complex_sparse_matrix& M) {
	// Generic type placeholders
	long int pLongInt;
    std::complex<double> pComplex;
	Py_complex complexPy; // struct { double real; double imag } Py_complex;

	// Python Object placeholders for iterating
	PyObject* pPyItem;

	// Python Object placeholders for the complex sparse matrix
	PyObject* indptr;
	PyObject* indices;
	PyObject* data;
	PyObject* shape;
	PyObject* nnz;

	// Parsing the complex_sparse_matrix: MPy must be "scipy.sparse.csr_matrix"

	// indptr -> M.p
	if (!PyObject_HasAttrString(MPy, "indptr")) {
		return -1;
	}
	// Extract the object
	indptr = PyObject_GetAttrString(MPy, "indptr");
	// Initialize p component of M
	M.p = new long int[PyObject_Size(indptr)];
	// Parse the indptr part of "csr_matrix"
	for (int i = 0; i < PyObject_Size(indptr); i++) {
		pPyItem = PyObject_GetItem(indptr, PyLong_FromLong(i));
		if (!PyArg_Parse(pPyItem, "l", &pLongInt)) {
			return -1;
		}
		M.p[i] = pLongInt;
	}

	// indices -> M.i
	if (!PyObject_HasAttrString(MPy, "indices")) {
		return -1;
	}
	// Extract the object
	indices = PyObject_GetAttrString(MPy, "indices");
	// Initialize i component of M
	M.i = new long int[PyObject_Size(indices)];
	// Parse the indices part of "csr_matrix"
	for (int i = 0; i < PyObject_Size(indices); i++) {
		pPyItem = PyObject_GetItem(indices, PyLong_FromLong(i));
		if (!PyArg_Parse(pPyItem, "l", &pLongInt)) {
			return -1;
		}
		M.i[i] = pLongInt;
	}

	// data.real -> M.x, data.imag -> M.z
	if (!PyObject_HasAttrString(MPy, "data")) {
		return -1;
	}
	// Extract the object
	data = PyObject_GetAttrString(MPy, "data");
	// Initialize x and z component of M
	M.x = new double[PyObject_Size(data)];
	M.z = new double[PyObject_Size(data)];
    M.v = new std::complex<double>[PyObject_Size(data)];
	// Parse the data part of "csr_matrix"
	for (int i = 0; i < PyObject_Size(data); i++) {
		pPyItem = PyObject_GetItem(data, PyLong_FromLong(i));
		if (!PyArg_Parse(pPyItem, "D", &complexPy)) {
			return -1;
		}
		M.x[i] = complexPy.real;
		M.z[i] = complexPy.imag;
        pComplex.real(M.x[i]);
        pComplex.imag(M.z[i]);
        M.v[i] = pComplex;
	}

	// rows -> M.rows, cols -> M.cols
	if (!PyObject_HasAttrString(MPy, "shape")) {
		return -1;
	}
	// Extract the object
	shape = PyObject_GetAttrString(MPy, "shape");
	// This is always a 2-tuple, with shape[0] == rows, shape[1] == cols
	if (!PyArg_Parse(PyTuple_GetItem(shape, 0), "l", &M.rows)) {
		return -1;
	}
	if (!PyArg_Parse(PyTuple_GetItem(shape, 1), "l", &M.cols)) {
		return -1;
	}
	// nnz -> M.nnz
	if (!PyObject_HasAttrString(MPy, "nnz")) {
		return -1;
	}
	nnz = PyObject_GetAttrString(MPy, "nnz");
	// Extract the object
	if (!PyArg_Parse(nnz, "l", &M.nz)) {
		return -1;
	}

	return 0;
}


int parse_encodedOps(PyObject* eOpsPy, EncodedOps& eOps) {
	// Allocating placeholders
	PyObject *item;
	int size = PyList_Size(eOpsPy);
	long int placeholder;
	EncodedOp eOp;

	for (int i = 0; i < size; i++) {
		item = PyList_GetItem(eOpsPy, i);
		// Each encoded op consists of 4 "flags"
		eOp = {0,0,0,0};
		// Going through each element of the encodedOp
		for (int j = 0; j < PyList_Size(item); j++) {
			if (!PyArg_Parse(PyList_GetItem(item, j), "l", &placeholder)) {
				return -1;
			}
			eOp[j] = placeholder;
		}
		eOps.push_back(eOp);
	}

	return 0;

}
