#!/usr/bin/python3
#
# This file is part of pynetworkred.
#
# Copyright (C) 2019, University of Copenhagen
# https://bitbucket.org/stefansommer/pynetworkred
# 
# pynetworkred is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# pynetworkred is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with networkred.  If not, see <http://www.gnu.org/licenses/>.
# 

from pykronred import kronred_symbolic, kronred, kronred_extract
from scipy.sparse import csr_matrix
import numpy as np

def print44Sparse(Ms):
    Mn = 4;
    Mdense = np.zeros((Mn, Mn))

    # Filling out the matrix
    indices = Ms.indices
    indptr = Ms.indptr
    data = Ms.data
    for i in range(Mn):
        ri = indptr[i]
        while(ri < indptr[i+1]):
            Mdense[i][indices[ri]] = data[ri].real
            ri += 1
    print(Mdense)

def print33Sparse(Ms):
    Mn = 3;
    Mdense = np.zeros((Mn, Mn))

    # Filling out the matrix
    indices = Ms.indices
    indptr = Ms.indptr
    data = Ms.data
    for i in range(Mn):
        ri = indptr[i]
        while(ri < indptr[i+1]):
            Mdense[i][indices[ri]] = data[ri].real
            ri += 1
    print(Mdense)

# define sparse 4x4 matrix
# M = [    1   0.1         0.4
#        0.1     2   0.2
#              0.2     3   0.3
#        0.4         0.3     4 ]

Mp = [0,3,6,9,12]
Mi = [0,1,3,0,1,2,1,2,3,0,2,3]
Mx = [1.0,0.1,0.4,0.1,2.0,0.2,0.2,3.0,0.3,0.4,0.3,4]
Mz = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0]

Mn = 4
Mnz = Mp[Mn]

# Pluggin everything into a "csr_matrix"
csm = csr_matrix((np.array(Mx) + 1j * np.array(Mz), Mi, Mp))

# Parameters for symbolic kronred
cutDegree = Mn
Nsweeps = 1
Nnvc = 1
split = False
updateEntireMatrix = True

csm_symbolic, nkr, index, eOps1, eOps2 = kronred_symbolic(csm,
                                                          Nnvc,
                                                          cutDegree,
                                                          Nsweeps,
                                                          split,
                                                          updateEntireMatrix)

print("M before reduction:")
print44Sparse(csm)
print("Symbolic M:")
print44Sparse(csm_symbolic)

print("Nkr", nkr)
print("index", index)
print("eOps1", eOps1)
print("eOps2", eOps2)

[Mkr] = kronred(csm_symbolic, eOps1, index)
print(Mkr)

Mxtemp = [x.real for x in Mkr.data]
Mztemp = [x.imag for x in Mkr.data]

print("Mxtemp:", Mxtemp)
print("Mztemp:", Mztemp)

Mn = 3
Mextract = kronred_extract(csm_symbolic, Mxtemp, Mztemp, index, Mn)
print33Sparse(Mextract)
