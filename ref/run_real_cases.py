#!/usr/bin/python3
#
# This file is part of pynetworkred.
#
# Copyright (C) 2019, University of Copenhagen
# https://bitbucket.org/stefansommer/pynetworkred
# 
# pynetworkred is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# pynetworkred is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with networkred.  If not, see <http://www.gnu.org/licenses/>.
# 

import scipy.io as sio
from scipy.sparse import csr_matrix, csc_matrix
import numpy as np
from thevenin_kronred import thevenin_kronred
from thevenin_parkronred import thevenin_parkronred
import time
import matplotlib.pyplot as plt

cases = sio.loadmat('data/matpower/ybus.mat');
res = sio.loadmat('data/result.mat');

time_taken_kronred = []
time_taken_parkronred = []
n = []
nnvc = []
print('Executing Thevenin Kronred test...')
for i, case in enumerate(cases['cases']):
#for i, case in enumerate([cases['cases'][16]]):
    format_str = ""
    name, ybus, generators = case[0][0][0]
    # Add name of example
    name = name[0][:-2] if len(name[0][:-2]) < 12 else (name[0][:-2])[:10] + '..'
    name = name if len(name) == 12 else name + " " * (12 - len(name))
    format_str += "grid: {0}".format(name)
    # Add number of nodes
    N = ybus.shape[0]
    format_str += " "*2 + "N: {0}".format(str(N) + " " * (5 - len(str(N))))
    # Add number of non-voltage controlled nodes
    Nnvc = N - np.unique(generators).shape[0]
    n.append(N)
    nnvc.append(Nnvc)
    format_str += " "*2 + "Nnvc: {0}".format(str(Nnvc) + " " * (5 - len(str(Nnvc))))
    # Add number of voltage controlled nodes
    Nvc = np.unique(generators).shape[0]
    format_str += " "*2 + "Nvc: {0}".format(str(Nvc) + " " * (5 - len(str(Nvc))))
    if i == 6 or i == 7:
        fname = str(N) + '_' + str(Nvc)
    else:
        fname = str(N)
    # The true thevenin impedances
    vs_true = sio.loadmat('data/validation/case' + fname + 'v.mat')['v'];
    # The matrices
    M = csc_matrix(sio.loadmat('data/full_matrices/case' + fname + 'M.mat')['full_M'].T)
    # Computing thevenin impedances
    t0 = time.time()
    vs = thevenin_kronred(M, N, Nnvc, Nvc, reduction=True)
    t1 = time.time()
    # Compute difference between true and computed impedances
    try:
        error = np.sum(np.abs(np.array(vs) - np.array(vs_true)))
        format_str += " "*2 + "Error: {0}".format(str(error))
    except:
        format_str += " "*2 + "Error: NAN"
    format_str += " "*2 + "Time: {0} s".format(t1 - t0)
    time_taken_kronred.append(t1 - t0)
    print(format_str)

print('Executing Thevenin Parkronred test...')
for i, case in enumerate(cases['cases']):
#for i, case in enumerate([cases['cases'][16]]):
    format_str = ""
    name, ybus, generators = case[0][0][0]
    # Add name of example
    name = name[0][:-2] if len(name[0][:-2]) < 12 else (name[0][:-2])[:10] + '..'
    name = name if len(name) == 12 else name + " " * (12 - len(name))
    format_str += "grid: {0}".format(name)
    # Add number of nodes
    N = ybus.shape[0]
    format_str += " "*2 + "N: {0}".format(str(N) + " " * (5 - len(str(N))))
    # Add number of non-voltage controlled nodes
    Nnvc = N - np.unique(generators).shape[0]
    format_str += " "*2 + "Nnvc: {0}".format(str(Nnvc) + " " * (5 - len(str(Nnvc))))
    # Add number of voltage controlled nodes
    Nvc = np.unique(generators).shape[0]
    format_str += " "*2 + "Nvc: {0}".format(str(Nvc) + " " * (5 - len(str(Nvc))))
    if i == 6 or i == 7:
        fname = str(N) + '_' + str(Nvc)
    else:
        fname = str(N)
    # The true thevenin impedances
    vs_true = sio.loadmat('data/validation/case' + fname + 'v.mat')['v'];
    # The matrices
    M = csc_matrix(sio.loadmat('data/full_matrices/case' + fname + 'M.mat')['full_M'].T)
    # Computing thevenin impedances
    t0 = time.time()
    vs = thevenin_parkronred(M, N, Nnvc, Nvc)
    t1 = time.time()
    # Compute difference between true and computed impedances
    try:
        error = np.sum(np.abs(np.array(vs) - np.array(vs_true)))
        format_str += " "*2 + "Error: {0}".format(str(error))
    except:
        format_str += " "*2 + "Error: NAN"
    format_str += " "*2 + "Time: {0} s".format(t1 - t0)
    time_taken_parkronred.append(t1 - t0)
    print(format_str)


# Plotting the results
fig, ax = plt.subplots()
ind = np.arange(1, len(n)+1)


argsort = np.argsort(nnvc)
nnvc = np.array(nnvc)[argsort]
time_taken_kronred = np.array(time_taken_kronred)[argsort]

ax.set_xticks(ind)
ax.set_xticklabels(list(map(lambda x: str(x), nnvc)))

plt.bar(ind, time_taken_kronred)
plt.tick_params(labelsize=12);
plt.xlabel('Number of non-voltage controlled nodes', fontsize=24)
plt.ylabel('Time [s]', fontsize=24)
plt.title('Computational time used by Thevenin Kronred', fontsize=24)
plt.show()


# Plotting parkronred results
fig, ax = plt.subplots()
ind = np.arange(1, len(n)+1)

time_taken_parkronred = np.array(time_taken_parkronred)[argsort]

ax.set_xticks(ind)
ax.set_xticklabels(list(map(lambda x: str(x), nnvc)))

plt.bar(ind, time_taken_parkronred)
plt.tick_params(labelsize=12);
plt.xlabel('Number of non-voltage controlled nodes', fontsize=24)
plt.ylabel('Time [s]', fontsize=24)
plt.title('Computational time used by Thevenin Par-Kronred', fontsize=24)
plt.show()

# Plotting MATLAB kronred results (maybe remove?)
fig, ax = plt.subplots()
ind = np.arange(1, len(n)+1)

time_taken_kronred = list(map(lambda x: float(x), open('matlab_time_kronred_results.csv').readlines()))
time_taken_kronred = np.array(time_taken_kronred)[argsort]

ax.set_xticks(ind)
ax.set_xticklabels(list(map(lambda x: str(x), nnvc)))

plt.bar(ind, time_taken_kronred)
plt.tick_params(labelsize=12);
plt.xlabel('Number of non-voltage controlled nodes', fontsize=24)
plt.ylabel('Time [s]', fontsize=24)
plt.title('Computational time used by MATLAB Kronred', fontsize=24)
plt.show()
