#!/usr/bin/python3
#
# This file is part of pynetworkred.
#
# Copyright (C) 2019, University of Copenhagen
# https://bitbucket.org/stefansommer/pynetworkred
# 
# pynetworkred is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# pynetworkred is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with networkred.  If not, see <http://www.gnu.org/licenses/>.
# 

from scipy.sparse import csr_matrix, csc_matrix
import numpy as np
from thevenin_ref import thevenin_ref
from thevenin_ref_klu import thevenin_ref_klu
from thevenin_kronred import thevenin_kronred
from thevenin_parkronred import thevenin_parkronred

Mp = [0,4,8,12,16,20]
Mi = [0,1,4,1,2,4,1,2,3,2,3,4,1,3,4]
Mx = [1.0,0.4,1.0,1.0,0.1,0.4,0.1,2.0,0.2,0.2,3.0,0.3,0.4,0.3,4]
Mz = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0,0,0,0]

Mn = 5
Mnz = Mp[Mn]

M = [[1.0+0.j, 0.+0.j, 0.4+0.j, 1.+0.j, 0.+0.j],
     [0.+0.j, 1.+0.j, 0.1+0.j, 0.+0.j, 0.4+0.j],
     [0.4+0.j, 0.1+0.j, 2.+0.j, 0.2+0.j, 0.+0.j],
     [1.+0.j, 0.+0.j, 0.2+0.j, 3.+0.j, 0.3+0.j],
     [0.+0.j, 0.4+0.j, 0.+0.j, 0.3+0.j, 4.+0.j]]

#csm = csc_matrix((np.array(Mx) + 1j * np.array(Mz), Mi, Mp))
csm = csc_matrix(np.array(M))

j = 4

# Thevenin impedances from the reference solution
impedance_ref = thevenin_ref(csm, Mn, Mn-j, j)
print("Impedances of ref:\n", impedance_ref)

# Thevenin impedances of the KLU reference solution
impedance_ref_klu = thevenin_ref_klu(csm, Mn, Mn-j, j)
print("Impedances of ref using KLU:\n", impedance_ref_klu)

# Thevenin impedances of the thevenin kronred solution
impedance_kronred = thevenin_kronred(csm, Mn, Mn-j, j)
print("Impedances of kronred solution (with reduction):\n", impedance_kronred)

# Thevenin impedances of the thevenin kronred solution
impedance_kronred = thevenin_kronred(csm, Mn, Mn-j, j, False)
print("Impedances of kronred solution (without reduction):\n", impedance_kronred)

# Thevenin impedances of the thevenin parkronred solution
impedance_parkronred = thevenin_parkronred(csm, Mn, Mn-j, j)
print("Impedances of parkronred solution:\n", impedance_parkronred)
