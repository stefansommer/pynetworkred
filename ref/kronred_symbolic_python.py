#
# This file is part of pynetworkred.
#
# Copyright (C) 2019, University of Copenhagen
# https://bitbucket.org/stefansommer/pynetworkred
# 
# pynetworkred is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# pynetworkred is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with networkred.  If not, see <http://www.gnu.org/licenses/>.
# 

from scipy.linalg import norm
from scipy.sparse.linalg import splu, spsolve_triangular
import numpy as np


def kronred_symbolic_python(M, Nnvc, cutDegree, Msymbolic, oldindex, split, updateEntireMatrix,
        ops1, ops2):
    Mkr = M.todense()
    Nnvckr = Nnvc
    N = M.shape[0]

    index = oldindex;

    krI = 0; # Index into Mkr
    for i in range(Nnvc):
        node = oldindex[i]
        col = Mkr[:, krI]
        degree = np.count_nonzero(col[0:Nnvckr]) - 1

        if degree <= cutDegree: # cut
            filter_ = np.zeros(col.shape, dtype=int)
            filter_[krI] = 1
            reverse_filter = (np.ones(col.shape, dtype=int) - filter_) > 0
            reverse_filter = reverse_filter.flatten()

            # Update
            row = Mkr[krI, :]
            update = col * row / col[krI]

            Mkr = Mkr - update
            Mkr = Mkr[reverse_filter]

            # Whats going on here? This is not equivalent is it?
            if krI == 0:
                index = index[krI+1:]
            else:
                index = index[0:krI-1] + index[krI+1:]
