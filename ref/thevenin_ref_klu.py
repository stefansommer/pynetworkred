#
# This file is part of pynetworkred.
#
# Copyright (C) 2019, University of Copenhagen
# https://bitbucket.org/stefansommer/pynetworkred
# 
# pynetworkred is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# pynetworkred is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with networkred.  If not, see <http://www.gnu.org/licenses/>.
# 

from scipy.linalg import norm
from scipy.sparse.linalg import splu, spsolve_triangular
import numpy as np
from pythevenin import thevenin_klu

'''
@Fill this in with some nice comment :-)

@param M: A matrix - Must be ordered non-voltage controlled first.
@param N: The number of total nodes (i.e. rows, where rows == cols)
@param Nnvc: Number of non-voltage controlled nodes
@param Nvc: Number of voltage controlled nodes
@return: v - the thevenin impedances, timetotal, timelu,
    timekronred, timebacksolve
'''
def thevenin_ref_klu(M, N, Nnvc, Nvc):
    # Non-voltage controlled indices of M
    Mnvc = M[0:Nnvc, 0:Nnvc]
    NITER = 1

    # Factorice Mnvc
    [u, l, p, q, _, _, _] = thevenin_klu(Mnvc, NITER)
    L = u.T
    U = l.T
    assert(norm(L.todense()*U.todense() - Mnvc) < 1e-10)
    # The array that will store the thevinin impedances
    v = []
    for k in range(Nvc):
        ystark = M[0:Nnvc, Nnvc+k]
        ystark = ystark[p]

        # Solves a system Ax = b (the "1" indicates that this is the lower triangular
        # part of the LU)
        ustark = spsolve_triangular(L, ystark.todense(), 1)

        # Calculates the 1-norm of a vector, which is the sum of the element magnitudes
        # and asserts that it is very small (otherwise the system of equations is
        # unstable)
        assert(norm(L*ustark-ystark, 1) < 1e-10)

        ykstar = M[Nnvc+k, 0:Nnvc].H
        ykstar = ykstar[q]
        lkstar = spsolve_triangular(U.H, ykstar.todense(), 1)

        assert(norm(U.H*lkstar-ykstar, 1) < 1e-10)

        uk = M[Nnvc+k,Nnvc+k]-lkstar.H * ustark

        v.append(1/uk)
    v = [np.complex(w) for w in v]
    return v
