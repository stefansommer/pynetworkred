#
# This file is part of pynetworkred.
#
# Copyright (C) 2019, University of Copenhagen
# https://bitbucket.org/stefansommer/pynetworkred
# 
# pynetworkred is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# pynetworkred is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with networkred.  If not, see <http://www.gnu.org/licenses/>.
# 

from scipy.linalg import norm
from scipy.sparse import csr_matrix, csc_matrix
from scipy.sparse.linalg import splu, spsolve_triangular
import numpy as np
from pythevenin import thevenin_klu, thevenin_klu_parkronred, thevenin_reach, thevenin_getz
from pykronred import kronred_symbolic, kronred
from kronred_symbolic_python import kronred_symbolic_python


def thevenin_parkronred(M, N, Nnvc, Nvc):
    # Parameters to the kronred_symbolic call
    NITER = 1
    cutDegree = 1
    Nsweeps = 3
    split = False
    updateEntireMatrix = False
    # The kronred_symbolic call
    Msymbolic, Nkr, index, eOps1, eOps2 = kronred_symbolic(M, Nnvc, cutDegree, Nsweeps, split,
                                                                   updateEntireMatrix)
    # The kronred call
    [Mkr] = kronred(Msymbolic, eOps1, index)
    eOps2 = [[0,0,0,0]] if eOps2 == [] else eOps2

    # KLU part
    M = Mkr
    N = M.shape[1]
    Nnvc = Nkr-Nvc;

    nvc = [i for i in range(Nnvc)]
    vc = [i for i in range(Nnvc, N)]

    # If there are no non-voltage controlled nodes, we're done
    if Nnvc == 0:
        v = np.array(list(map(lambda x: x.real, 1. / M[vc].diagonal())))
        return v.reshape((v.shape[0], 1))

    Mnvc = M[:Nnvc, :Nnvc]

    # The KLU factorization
    [u, l, p, q, _, _, _] = thevenin_klu_parkronred(Mnvc, Mkr, eOps2, NITER)

    L = u.T
    U = l.T
    Mvc = M[Nnvc:N, Nnvc:N]

    link  = csc_matrix(M[:Nnvc, Nnvc:])
    linkT = csr_matrix(M[Nnvc:, :Nnvc]).H

    reachL = thevenin_reach(Nnvc, Nvc, L, link, q)
    reachU = thevenin_reach(Nnvc, Nvc, csc_matrix(U.H), linkT, p)

    reachL = list(np.array(reachL).T.flatten())
    reachU = list(np.array(reachU).T.flatten())
    diagOnly = True

    vfast = thevenin_getz(Nnvc, Nvc, L, csc_matrix(U.H), link, linkT, Mvc, q, p, reachL,
            reachU, diagOnly)
    return np.reshape(vfast, (len(vfast), 1))
