#
# This file is part of pynetworkred.
#
# Copyright (C) 2019, University of Copenhagen
# https://bitbucket.org/stefansommer/pynetworkred
# 
# pynetworkred is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# pynetworkred is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with networkred.  If not, see <http://www.gnu.org/licenses/>.
# 

from scipy.linalg import norm, lu
from scipy.sparse.linalg import splu, spsolve_triangular
from scipy.sparse import csr_matrix, csc_matrix
import numpy as np

'''
Function for computing thevenin impedance - however, this is not the best way to do it
and should simply be seen as a proof of concept.
This file is meant to produce the same results as "thevenin_ref.m",
which is originally written by Stefan Sommer (sommer@di.ku.dk), and which can be found at:
https://bitbucket.org/stefansommer/networkred

This method is also called "factor-solve Thevenin impedance calculation".
@param M: A matrix - Must be ordered non-voltage controlled first.
@param N: The number of total nodes (i.e. rows, where rows == cols)
@param Nnvc: Number of non-voltage controlled nodes
@param Nvc: Number of voltage controlled nodes
@return: v - the thevenin impedances, timetotal (0 in this case), timelu (0 in this case),
    timekronred (0 in this case), timebacksolve (0 in this case)
'''
def thevenin_ref(M, N, Nnvc, Nvc):
    # Non-voltage controlled indices of M
    Mnvc = M[0:Nnvc, 0:Nnvc]

    # Factorice Mnvc
    LU = splu(Mnvc)
    L = LU.L
    U = LU.U

    # LU contains L in the lower triangle and U in the upper
    L = LU.L.tocsr() # Needed for efficiency of triangular solve
    U = LU.U.tocsr() # Needed for efficiency of triangular solve
    r, _ = Mnvc.shape
    # The row permutation of the LU factorization
    p = LU.perm_r
    # The column permutation of the LU factorization
    q = LU.perm_c
    # The permutation matrices
    Pr = csc_matrix((r, r))
    Pr[p, np.arange(r)] = 1

    Pc = csc_matrix((r, r))
    Pc[np.arange(r), q] = 1
    # Asserting that the LU factorisation was successful
    #assert norm((Pr.T * (L * U) * Pc.T).todense() - Mnvc) < 1e-10
    assert norm(((Pr.T * L * Pc.T) * (Pr.T * U * Pc.T)).todense() - Mnvc) < 1e-10
    # The array that will store the thevenin impedances
    v = []
    for k in range(Nvc):
        ystark = M[0:Nnvc, Nnvc+k]
        ystark = ystark

        # Solves a system Ax = b (the "1" indicates that this is the lower triangular
        # part of the LU)

        ustark = spsolve_triangular(L, ystark.todense(), 1)

        # Calculates the 1-norm of a vector, which is the sum of the element magnitudes
        # and asserts that it is very small (otherwise the system of equations is
        # unstable)
        assert norm(L*ustark-ystark, 1) < 1e-10

        ykstar = M[Nnvc+k, 0:Nnvc].H
        ykstar = ykstar
        lkstar = spsolve_triangular(U.H, ykstar.todense(), 1)

        #assert(norm(U*lkstar-ykstar, 1) < 1e-10)

        uk = M[Nnvc+k, Nnvc+k]-lkstar.H * ustark

        v.append(1/uk)
    v = [np.complex(w) for w in v]
    return v
